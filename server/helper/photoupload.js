const util = require('util');
const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');

let storage = new GridFsStorage({
    url: 'mongodb+srv://root:cvkLK6AAm9o4ovpW@cluster0.gqqio.mongodb.net/<dbname>?retryWrites=true&w=majority',
    options: {useNewUrlParser: true, useUnifiedTopology: true},
    file: (req, file)=> {
        const match = ['image/png', 'image/jpeg'];

        if(match.indexOf(file.mimetype) === -1){
            const filename = `${Date.now()}-${file.originalname}`

            return filename;
        }
        return{
            bucketName: "photos",
            filename: `${Date.now()}-${file.originalname}`
        };
    }
});

let uploadFile = multer({ storage: storage}).single("file");
let uploadFilesMiddlewares = util.promisify(uploadFile);

module.exports = uploadFilesMiddlewares;