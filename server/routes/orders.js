const {Order} = require('../models/orders');
const express = require('express');
const {OrderItem} = require('../models/order_item')
const router = express.Router();

router.get(`/`, async (req, res) =>{
    const orderList = await Order.find().populate('user', 'userName');

    if(!orderList) {
        res.status(500).json({success: false})
    } 
    res.send(orderList);
})

router.get(`/:id`, async (req, res) =>{
   try{
    const order = await Order.findById(req.params.id)
    .populate('user', 'userName')
    .populate({ 
        path: 'orderItems', populate: { 
            path: 'product', populate: 'category'
        }
     });

     res.status(201).json({
         success: true,
         message: 'Order retrieved succesfully',
         order: order
     });
   }catch(err){ 
    if(!order) {
        res.status(500).json({
            success: false,
            message: 'No order available'
        })
    } 
    
   }

    
})

router.post(`/`, async (req, res) => {
    try{
        const orderItemsIds = Promise.all(req.body.orderItems.map(
            async orderItem => {
                let newOrderItem = new OrderItem({
                    quantity: orderItem.quantity,
                    product: orderItem.product
                });

                newOrderItem = await newOrderItem.save();

                return newOrderItem._id;
            }
        ))
        const orderItemsIdsResolved = await orderItemsIds;
        
        const totalPrices =  await Promise.all(orderItemsIdsResolved.map(async (orderItemsId) => {
            const orderItem = await OrderItem.findById(orderItemsId).populate('product', 'price');
            const totalPrice = orderItem.product.price * orderItem.quantity;
            return totalPrice;
        }))

        const totalPrice = totalPrices.reduce((a,b) => a+b, 0);
        console.log(totalPrices);

        let order = new Order({
          orderItems: orderItemsIdsResolved,
          shippingAddress1: req.body.shippingAddress1,
          shippingAddress2: req.body.shippingAddress2,
          city: req.body.city,
          zip: req.body.zip,
          country: req.body.country,
          phone: req.body.phone,
          status: req.body.status,
          totalPrice: totalPrice,
          user: req.body.user,  
        });

         order = await order.save()
        res.json({
            success: true,
            message: "Successfully created a new order!",
            order: order
        });

    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.put(`/:id`, async (req, res) => {
    try{
        const orderUpdated = await Order.findByIdAndUpdate(
            req.params.id,
        {
           status: req.body.status,
        },
        { new: true}

        
    )
    res.json({
        success: true,
        message: "Successfully updated your order!",
        orderUpdated: orderUpdated
    })
    }catch(err){
        return res.status(400).json({
            success: false,
            message: 'No order found',

        })
    }
});

router.delete(`/:id`, (req, res) => {
    try{
        Order.findByIdAndDelete(req.params.id).then(async deletedOrder => {
            if(deletedOrder) {
                await deletedOrder.orderItems.map(async orderItem => {
                    await OrderItem.findByIdAndDelete(orderItem)
                })
                return res.status(200).json({
                    success: true,
                    message: 'Order deleted successfully!',
                    
                })
            } else {
                return res.status(404).json({
                    success: false,
                    message: 'No such order found'
                });
            }
        });
    }catch(errr){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.get(`/get/totalsales`, async (req, res) => {
    const totalSales = await Order.aggregate([
        { $group: { _id: null, totalSales: { $sum: '$totalPrice'}}}
    ]);

    if(!totalSales) {
        return res.status(400).json({
            success: false,
            message: 'Order cannot be generated'
        });
        
    }
    res.send({
        totalSales: totalSales.pop().totalSales
    })
});

router.get(`/get/count`, async (req, res) => {
    
    const orderCount = await Order.countDocuments((count) => count)
    if(!orderCount) {
        res.status(500).json({
            success: false,
            message: 'No products available'
        });
    }
    res.send({
        orderCount: orderCount
    });
});

router.get(`/get/userorders/:userid`, async (req, res) => {
    const userOrder = await Order.find({ user: req.params.userid}).populate({ 
        path: 'orderItems', populate: { 
            path: 'product', populate: 'category'
        }
     });

     if(!userOrder) {
         res.status(500).json({
             success: false,
             message: 'No order found',
         });
     };

     res.status(201).json({
         success: true,
         message: 'User order found successfully!',
         userOrder: userOrder
     })
})

module.exports = router;