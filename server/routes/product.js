const router = require('express').Router()
const Product = require('../models/product')
const mongoose = require('mongoose')
const multer = require('multer');
const product = require('../models/product');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
      cb(null, new Date().toISOString() + file.originalname);
    }
  });
  
  const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });

router.post(`/`, upload.single('image'), async(req, res) => {
    const file = req.file;

    const fileName = file.filename
    const basePath = `${req.protocol}://${req.get('host')}/uploads/`;

    let product = new Product();
    try{
        product.title = req.body.title;
        product.description = req.body.description;
        product. richDescription = req.body.richDescription;
        product.image = `${basePath}${fileName}`;
        product.brand = req.body.brand;
        product.price = req.body.price;
        product.category = req.body.category;
        product.stockQuantity = req.body.stockQuantity;
        product.rating = req.body.rating;
        product.numReviews = req.body.numReviews;
        product.isFeatured = req.body.isFeatured;
        await product.save();

        res.json({
            stats: true,
            message: "Successfully saved!",
            product: product
        })

    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })

    }

});

router.get(`/`, async (req, res) => {
    try{
        let filter = {};
        if(req.query.categories){
             filter = req.query.categories.split(',');
        }
        let products = await Product.find(filter).populate('category'); 
        res.json({
            success:  true,
            products: products
        });
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.get(`/:id`, async (req, res) => {
    try{
        let product = await Product.findById(req.params.id).populate('category'); 
        res.json({
            success:  true,
            product: product
        });
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.put(`/:id`, async (req, res) => {
    try{
        
        if(!mongoose.isValidObjectId(req.params.id)){
            return res.status(500).send('Invalid product Id')
        }
        // const category = await Category.findById(req.body.category);
        
        // if(!category) return res.status(500).send('Invalid category');
        let product = await Product.findByIdAndUpdate( req.params.id,{
            title: req.body.title,
            description: req.body.description,
            richDescription: req.body.richDescription,
            image: req.body.image,
            brand: req.body.brand,
            price: req.body.price,
            category: req.body.category,
            stockQuantity: req.body.stockQuantity,
            rating: req.body.rating,
            numReviews: req.body.numReviews,
            isFeatured: req.body.isFeatured,
            dateCreated: req.body.dateCreated,

        },{
            new: true
        });
       
        res.json({
            success: true,
            product: product
        })
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
})

router.delete(`/:id`, async (req, res) => {
    try{
        let deletedProduct = await Product.findOneAndDelete({_id: req.params.id});
        
        if(deletedProduct){
            res.status(201).json({
                success: true,
                message: 'Successfully deleted product'
            })
        }
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
})

router.get(`/get/count`, async (req, res) => {
    
        const productCount = await Product.countDocuments((count) => count)
        if(!productCount) {
            res.status(500).json({
                success: false,
                message: 'No products available'
            });
        }
        res.send({
            productCount: productCount
        });
});

router.get(`/get/featured/:count`, async (req, res) => {
    const count = req.params.count ? req.params.count : 0
    const products = await Product.find({isFeatured: true}).limit(+count)
    if(!products) {
        res.status(500).json({
            success: false,
            message: 'No products available'
        })
    }
    res.send({
        products: products
    })
});

router.put('/gallery-images/:id', 
            upload.any('images', 10), 
            async (req, res) => {
                if(!mongoose.isValidObjectId(req.params.id)) {
                    return res.status(400).send('Invalid Product Id')
                 }
                 const files = req.files
                 let imagesPaths = [];
                 const basePath = `${req.protocol}://${req.get('host')}/uploads/`;
        
                 if(files) {
                    files.map(file =>{
                        imagesPaths.push(`${basePath}${file.filename}`);
                    })
                 }
        
                 const product = await Product.findByIdAndUpdate(
                    req.params.id,
                    {
                        images: imagesPaths
                    },
                    { new: true}
                )
        
                if(!product)
                    return res.status(500).send('the gallery cannot be updated!')
        
                res.send(product);
        
    });

module.exports = router;
