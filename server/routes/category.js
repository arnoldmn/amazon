const router = require('express').Router();
const Category = require('../models/category');
const { post } = require('./product');
const { json } = require('body-parser');

router.post("/", async (req, res) => {
    try{
        const category = new Category();
        category.type = req.body.type;
        category.icon = req.body.icon;
        category.color = req.body.color;

        await category.save();
        res.json({
            success: true,
            message: "Successfully created a new category!"
        });

    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.get("/", async (req, res) => {
    try{
        let categories = await Category.find(); 
        res.json({
            success:  true,
            categories: categories
        });
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.get("/:id", async (req, res) => {
    try{
        let category = await Category.findOne({ _id: req.params.id }); 
        res.json({
            success:  true,
            category: category
        });
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.put('/:id', async (req, res) => {
    try{
        console.log(req.body);
        let category = await Category.findByIdAndUpdate(req.params.id,{
            type: req.body.type,
            icon: req.body.icon,
            color: req.body.color,
        }, {
            new: true
        });

        res.json({
            success: true,
            category: category
        })
        
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
})

router.delete('/:id', async (req, res) => {
    try{
        let deletedCategory = await Category.findOneAndDelete({_id: req.params.id});
        
        if(deletedCategory){
            res.json({
                status: true,
                message: 'Successfully deleted category'
            })
        }
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
})
module.exports = router;
