const router = require('express').Router();
const User = require('../models/users');
const { post } = require('./product');
const { json } = require('body-parser');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


router.post(`/`, async (req, res) => {
    try{
        let user = new User({
            userName: req.body.userName,
            email: req.body.email,
            passwordHash: bcrypt.hashSync(req.body.password, 10),
            phone: req.body.phone,
            street: req.body.street,
            isAdmin: req.body.isAdmin,
            apartment: req.body.apartment,
            zip: req.body.zip,
            city: req.body.city,
            country: req.body.country,
        });
        
        user = await user.save();
        res.json({
            success: true,
            message: "Successfully created a new user!",
            user: user
        });

    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.get(`/`, async (req, res) => {
    try{
        let users = await User.find().select('-passwordHash'); 
        res.json({
            success:  true,
            users: users
        });
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.get(`/:id`, async (req, res) => {
    try{
        let user = await User.findOne({ _id: req.params.id }).select('-passwordHash'); 
        res.json({
            success:  true,
            user: user
        });
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});

router.put(`/:id`, async (req, res) => {
    try{
        console.log(req.body);
        let user = await User.findByIdAndUpdate(req.params.id,{
            userName :req.body.userName,
            email: req.body.email,
            passwordHash: req.body.passwordHash,
            phone: req.body.phone,
            street: req.body.street,
            isAdmin: req.body.isAdmin,
            apartment: req.body.apartment,
            zip: req.body.zip,
            city: req.body.city,
            country: req.body.country,
        }, {
            new: true
        });

        res.json({
            success: true,
            user: user
        })
        
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
})

router.delete(`/:id`, async (req, res) => {
    try{
        let deletedUser= await User.findOneAndDelete({_id: req.params.id});
        
        if(deletedUser){
            res.json({
                status: true,
                message: 'Successfully deleted category'
            })
        }
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
})

router.get(`/get/count`, async (req, res) => {
    
    const userCount = await User.countDocuments((count) => count)
    if(!userCount) {
        res.status(500).json({
            success: false,
            message: 'No users available'
        });
    }
    res.send({
        userCount: userCount
    });
})

router.post(`/login`, async (req,res) => {
    try{
        
        const user = await User.findOne({email: req.body.email})
    const secret = process.env.secret;
    if(!user) {
        return res.status(400).send('The user not found');
    }

    if(user && bcrypt.compareSync(req.body.password, user.passwordHash)) {
        const token = jwt.sign(
            {
                userId: user.id,
                isAdmin: user.isAdmin
            },
            secret,
            {expiresIn : '1d'}
        )
            
        res.status(200).send({
            message: 'Successfully logged in...',
            user: user.email , 
            token: token
        }) 
    } else {
       res.status(400).send('Invalid credentials.');
    }
    }catch(err){

    }

    
})

router.post(`/register`, async (req, res) => {
    try{
        let user = new User({
            userName: req.body.userName,
            email: req.body.email,
            passwordHash: bcrypt.hashSync(req.body.password, 10),
            phone: req.body.phone,
            isAdmin: req.body.isAdmin,
            street: req.body.street,
            apartment: req.body.apartment,
            zip: req.body.zip,
            city: req.body.city,
            country: req.body.country   
        })
        user = await user.save();
        res.json({
            success: true,
            message: "Successfully registered as a new user!",
            user: user
        });
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        });
    };
})


module.exports = router;
