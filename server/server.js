const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const authJwt = require('./helper/jwt')
const errorHandling = require('./helper/error-handling')

const dotenv = require('dotenv');

dotenv.config();

 const app = express();

 mongoose.connect(
     process.env.DATABASE, 
    { useNewUrlParser: true, useUnifiedTopology: true }, err => {
    if(err) {
        console.log(err)
    } else{
        console.log("Connected to the database!");
    }
     
 });

 const PORT = process.env.PORT || 6000;
 const api = process.env.API_URL;
 app.use(cors());
 app.options('*', cors());

 app.use(morgan('tiny'));

 app.use(bodyParser.json());
 app.use(bodyParser.urlencoded({ extended: false }));
 app.use(authJwt());
 app.use('/uploads', express.static(__dirname + '/uploads'))
 app.use(function errorHanlding(err, req, res, next) {
    if(err.name === 'UnauthorizedError') {
            return res.status(401).json({
            message: 'User not authorized'
        });
    };

    if(err.name === 'ValidationError'){
        return res.status(401).json({message: err});
    }

    return res.status(500).json(err);
})

 const productRoute = require("./routes/product");
 const categoryRoute = require("./routes/category");
 const ownerRoute = require("./routes/owner");
 const uploadRoute = require("./controller/upload");
 const orderRoute = require("./routes/orders");
 const userRouter = require("./routes/users");


 app.use(`${api}/products`, productRoute);
 app.use(`${api}/categories`, categoryRoute);
 app.use(`${api}/owner`, ownerRoute);
 app.use(`${api}/orders`, orderRoute);
 app.use(`${api}/users`, userRouter);

 
 app.listen(PORT, err => {
     if(err) {
         console.log(err);
     }else{
         console.log(`Listening on port ${PORT}`);
     }
 });

 